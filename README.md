# Advent of Code 2018

This is a repository setup to practice Mob Programming as a team.

The [Advent of Code 2018](https://adventofcode.com/2018) provides the programming challenges to be tackled as a mob.
